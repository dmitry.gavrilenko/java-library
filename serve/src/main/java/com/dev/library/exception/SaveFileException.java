package com.dev.library.exception;

import org.springframework.http.HttpStatus;

public class SaveFileException extends GlobalException {
	public SaveFileException() {
	}

	public SaveFileException(String msg) {
		super(msg);
	}

	public SaveFileException(HttpStatus status, String msg, int code) {
		super(status, msg, code);
	}
}
