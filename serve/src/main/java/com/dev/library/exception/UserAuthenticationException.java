package com.dev.library.exception;

import org.springframework.http.HttpStatus;

public class UserAuthenticationException extends GlobalException {
	public UserAuthenticationException() {
	}

	public UserAuthenticationException(String msg) {
		super(msg);
	}

	public UserAuthenticationException(HttpStatus status, String msg, int code) {
		super(status, msg, code);
	}
}
