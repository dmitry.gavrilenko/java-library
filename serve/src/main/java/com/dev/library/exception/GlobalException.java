package com.dev.library.exception;

import org.springframework.http.HttpStatus;

public class GlobalException extends RuntimeException {

	private HttpStatus status;

	private String msg;

	private int code;

	public GlobalException() {
	}

	public GlobalException(String msg) {
		this.msg = msg;
	}

	public GlobalException(HttpStatus status, String msg, int code) {
		this.status = status;
		this.msg = msg;
		this.code = code;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
