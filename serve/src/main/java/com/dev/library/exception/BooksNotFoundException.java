package com.dev.library.exception;

import org.springframework.http.HttpStatus;

public class BooksNotFoundException extends GlobalException{
	public BooksNotFoundException() {
	}

	public BooksNotFoundException(String msg) {
		super(msg);
	}

	public BooksNotFoundException(HttpStatus status, String msg, int code) {
		super(status, msg, code);
	}
}
