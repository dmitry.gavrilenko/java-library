package com.dev.library.service;

import com.dev.library.domain.Author;
import com.dev.library.dto.ResponseAuthorDTO;
import com.dev.library.repository.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorService {

	private final AuthorRepository authorRepository;

	private final  ModelMapper modelMapper;

	public AuthorService(AuthorRepository authorRepository, ModelMapper modelMapper) {
		this.authorRepository = authorRepository;
		this.modelMapper = modelMapper;
	}

	public Author addAuthor(Author author) {
		return authorRepository.findAuthorByName(author.getName())
				.orElseGet(() -> authorRepository.save(author));
	}

	public List<ResponseAuthorDTO> findAllAuthors(){
		return authorRepository.findAll().stream()
				.map((author) -> modelMapper.map(author, ResponseAuthorDTO.class))
				.collect(Collectors.toList());
	}

}
