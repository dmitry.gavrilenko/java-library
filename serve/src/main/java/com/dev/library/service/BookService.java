package com.dev.library.service;

import com.dev.library.domain.Book;
import com.dev.library.domain.User;
import com.dev.library.dto.BookDTO;
import com.dev.library.dto.CriteriaDTO;
import com.dev.library.dto.ResponseBookDTO;
import com.dev.library.dto.ResponseDTO;
import com.dev.library.exception.BooksNotFoundException;
import com.dev.library.exception.SaveFileException;
import com.dev.library.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

	private final ModelMapper modelMapper;

	private final  BookRepository bookRepository;

	private final GanreService ganreService;

	private final AuthorService authorService;

	private final PublisherService publisherService;

	public BookService(ModelMapper modelMapper, BookRepository bookRepository, GanreService ganreService,
					   AuthorService authorService, PublisherService publisherService) {
		this.modelMapper = modelMapper;
		this.bookRepository = bookRepository;
		this.ganreService = ganreService;
		this.authorService = authorService;
		this.publisherService = publisherService;
	}

	@Transactional
	public ResponseDTO addBook(BookDTO bookDTO, User user) {
		Book book = modelMapper.map(bookDTO, Book.class);
		book.setGanres(ganreService.addGanre(book.getGanres()));
		book.setPublisher(publisherService.addPublisher(book.getPublisher()));
		book.setAuthor(authorService.addAuthor(book.getAuthor()));
		book.setUser(user);
		book.setBookPath(saveFile(bookDTO.getBook(), "books", bookDTO.getTitle() + ".pdf"));
		book.setCoverPath(saveFile(bookDTO.getCover(), "covers", bookDTO.getTitle() + ".jpg"));
		bookRepository.save(book);
		return new ResponseDTO(HttpStatus.OK, "Book added successfully", HttpStatus.OK.value());
	}

	public List<ResponseBookDTO> findBooksByCriteria(CriteriaDTO criteriaDTO) {
		return bookRepository.findBooksByAuthorsInAndGanresIn(criteriaDTO.getAuthorNames(), criteriaDTO.getGanreNames())
				.stream().map(book -> modelMapper.map(book, ResponseBookDTO.class)).collect(Collectors.toList());
	}

	public List<ResponseBookDTO> findAllBook() {
		return bookRepository.findAll()
				.stream()
				.map((book) -> modelMapper.map(book, ResponseBookDTO.class))
				.collect(Collectors.toList());
	}

	public byte[] getBookByBytes(String title) {
		File book = new File("books/" + title);
		byte[] bytes = new byte[(int) book.length()];
		try(FileInputStream fileInputStream = new FileInputStream(book)) {
			fileInputStream.read(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bytes;
	}

	private String saveFile(MultipartFile multipartFile, String directory, String title) {
		File dir = new File(directory);
		dir.mkdir();
		File book = new File(dir + "/" + title);
		try (FileOutputStream fio = new FileOutputStream(book)) {
			fio.write(multipartFile.getBytes());
		} catch (IOException e) {
			throw new SaveFileException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return book.getPath();
	}

	public ResponseDTO rate(long id) {
		Book book = bookRepository.findById(id).orElseThrow(
				() -> new BooksNotFoundException(HttpStatus.NOT_FOUND, "Book not found", HttpStatus.NOT_FOUND.value()));
		book.setRate(book.getRate() + 1);
		bookRepository.save(book);
		return new ResponseDTO(HttpStatus.OK,
				"Your like very important for us thank you", HttpStatus.OK.value());
	}

}
