package com.dev.library.service;

import com.dev.library.domain.Ganre;
import com.dev.library.dto.ResponseGanreDTO;
import com.dev.library.repository.GanreRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GanreService {

	private final GanreRepository ganreRepository;

	private final ModelMapper modelMapper;

	public GanreService(GanreRepository ganreRepository, ModelMapper modelMapper) {
		this.ganreRepository = ganreRepository;
		this.modelMapper = modelMapper;
	}

	public List<Ganre> addGanre(List<Ganre> ganres) {
		List<Ganre> ganreList = new ArrayList<>();
		ganres.forEach(ganre -> ganreList.add(
				ganreRepository.findGanreByName(ganre.getName()).orElseGet(() -> ganreRepository.save(ganre))));
		return ganreList;
	}

	public List<ResponseGanreDTO> findAllGanres(){
		return ganreRepository.findAll().stream()
				.map((author) -> modelMapper.map(author, ResponseGanreDTO.class))
				.collect(Collectors.toList());
	}

}
