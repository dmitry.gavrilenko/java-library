package com.dev.library.service;

import com.dev.library.domain.User;
import com.dev.library.dto.ResponseDTO;
import com.dev.library.dto.SignInDTO;
import com.dev.library.dto.SignUpDTO;
import com.dev.library.dto.TokenDTO;
import com.dev.library.exception.UserAuthenticationException;
import com.dev.library.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Service
public class ManagementUserService {

	private final UserRepository userRepository;

	private final ModelMapper modelMapper;

	private final PasswordEncoder passwordEncoder;

	private final JwtService jwtService;

	public ManagementUserService(UserRepository userRepository, ModelMapper modelMapper,
								 PasswordEncoder passwordEncoder, JwtService jwtService) {
		this.userRepository = userRepository;
		this.modelMapper = modelMapper;
		this.passwordEncoder = passwordEncoder;
		this.jwtService = jwtService;
	}

	public TokenDTO signIn(SignInDTO signInDTO) throws UnsupportedEncodingException {
		User user = userRepository.findUserByEmail(
				signInDTO.getEmail()).orElseThrow(() -> new UserAuthenticationException(HttpStatus.BAD_REQUEST,
				"Wrong password or email", HttpStatus.BAD_REQUEST.value()));
		if(!passwordEncoder.matches(signInDTO.getPassword(), user.getPassword())) {
			throw new UserAuthenticationException(HttpStatus.BAD_REQUEST,
					"Wrong password or email", HttpStatus.BAD_REQUEST.value());
		}
		return new TokenDTO(jwtService.generateToken(user));
	}

	public ResponseDTO signUp(SignUpDTO signUpDTO) {
		User user = modelMapper.map(signUpDTO, User.class);
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		return new ResponseDTO(HttpStatus.CREATED,
				"User saved successfully", HttpStatus.CREATED.value());
	}
}
