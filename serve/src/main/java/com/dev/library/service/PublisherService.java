package com.dev.library.service;

import com.dev.library.domain.Publisher;
import com.dev.library.dto.ResponsePublisherDTO;
import com.dev.library.repository.PublisherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PublisherService {

	private final ModelMapper modelMapper;

	private final PublisherRepository publisherRepository;

	public PublisherService(ModelMapper modelMapper,
							PublisherRepository publisherRepository) {
		this.modelMapper = modelMapper;
		this.publisherRepository = publisherRepository;
	}

	public Publisher addPublisher(Publisher publisher) {
		return publisherRepository.findPublisherByName(publisher.getName())
				.orElseGet(() -> publisherRepository.save(publisher));
	}

	public List<ResponsePublisherDTO> findAllPublishers() {
		return publisherRepository.findAll().stream()
				.map((author) -> modelMapper.map(author, ResponsePublisherDTO.class))
				.collect(Collectors.toList());
	}

}
