package com.dev.library.service;

import com.dev.library.domain.Book;
import com.dev.library.domain.User;
import com.dev.library.dto.ResponseBookDTO;
import com.dev.library.dto.ResponseDTO;
import com.dev.library.dto.UpdateBookDTO;
import com.dev.library.exception.BooksNotFoundException;
import com.dev.library.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

	private final BookRepository bookRepository;

	private final ModelMapper modelMapper;

	public UserService(BookRepository bookRepository, ModelMapper modelMapper) {
		this.bookRepository = bookRepository;
		this.modelMapper = modelMapper;
	}

	public List<ResponseBookDTO> getAllUserBook(User user) {
		return bookRepository.findBooksByUser(user).orElseThrow(()
				-> new BooksNotFoundException(HttpStatus.NOT_FOUND, "You do not added book yet",
				HttpStatus.NOT_FOUND.value()))
				.stream()
				.map((book) -> modelMapper.map(book, ResponseBookDTO.class)).collect(Collectors.toList());
	}

	@RolesAllowed({"ROLE_ADMIN"})
	public ResponseDTO deleteBook(long id) {
		bookRepository.deleteById(id);
		return new ResponseDTO(HttpStatus.OK, "Book with id: " + id, HttpStatus.OK.value());
	}

	public ResponseDTO updateBook(UpdateBookDTO updateBookDTO) {
		Book book = modelMapper.map(updateBookDTO, Book.class);
		bookRepository.save(book);
		return new ResponseDTO(HttpStatus.OK,
				"Book was update successfully", HttpStatus.OK.value());
	}

}
