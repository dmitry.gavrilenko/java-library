package com.dev.library.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "com.dev.library.service")
@Import({AuditConfig.class, SecurityConfig.class})
public class ServiceConfig {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
