CREATE TABLE IF NOT EXISTS publisher
(
  id           SERIAL PRIMARY KEY,
  created_time Date,
  updated_time Date,
  name         VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS author
(
  id           serial primary key,
  created_time Date,
  updated_time Date,
  name         varchar(200),
  birthday     varchar(50)
);

CREATE TABLE IF NOT EXISTS ganre
(
  id           SERIAL PRIMARY KEY,
  created_time Date,
  updated_time Date,
  name         VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS dev_user
(
  id           SERIAL PRIMARY KEY,
  created_time Date,
  updated_time Date,
  name         VARCHAR(200),
  email        VARCHAR(200) UNIQUE ,
  password     VARCHAR(200),
  authority    VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS book
(
  id           SERIAL PRIMARY KEY,
  created_time Date,
  updated_time Date,
  book_path    VARCHAR(200),
  cover_path   VARCHAR(200),
  title        VARCHAR(200) NOT NULL,
  isbn         VARCHAR(255),
  date         varchar(50),
  rate         INTEGER,
  publisher_id INTEGER,
  author_id    INTEGER,
  user_id      INTEGER,
  FOREIGN KEY (publisher_id) REFERENCES publisher (id),
  FOREIGN KEY (author_id) REFERENCES author (id),
  FOREIGN KEY (user_id) REFERENCES dev_user (id)
);

CREATE TABLE IF NOT EXISTS book_ganres
(
  book_id  INTEGER REFERENCES book (id),
  ganre_id INTEGER REFERENCES ganre (id),
  CONSTRAINT book_ganres_pkey PRIMARY KEY (book_id, ganre_id)
);
