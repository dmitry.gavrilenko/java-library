package com.dev.library.repository;

import com.dev.library.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Long> {
	Optional<Author> findAuthorByName(String name);
	List<Author> findAuthorsByNameIn(List<String> names);
}
