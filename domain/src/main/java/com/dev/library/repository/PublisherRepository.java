package com.dev.library.repository;

import com.dev.library.domain.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {
	Optional<Publisher> findPublisherByName(String name);
	List<Publisher> findPublishersByNameIn(List<String> names);
}
