package com.dev.library.repository;

import com.dev.library.domain.Ganre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GanreRepository extends JpaRepository<Ganre, Long> {
	Optional<List<Ganre>> findGanresByNameIn(List<String> names);
	Optional<Ganre> findGanreByName(String name);
}
