package com.dev.library.repository;

import com.dev.library.domain.Author;
import com.dev.library.domain.Book;
import com.dev.library.domain.Ganre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class BookRepositoryImpl implements BookRepositoryCustom {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Book> findBooksByAuthorsInAndGanresIn(List<String> authors, List<String> ganres) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Book> cq = cb.createQuery(Book.class);
		Root<Book> book = cq.from(Book.class);
		Join<Book, Ganre> ganre = book.join("ganres");
		Join<Book, Author> author = book.join("author");
		if(ganres != null && !ganres.isEmpty()) {
			cq.where(ganre.get("name").in(ganres)).distinct(true);
		}
		if(authors != null && !authors.isEmpty()) {
			cq.where(author.get("name").in(authors)).distinct(true);
		}
		return entityManager.createQuery(cq).getResultList();
	}
}
