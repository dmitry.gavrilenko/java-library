package com.dev.library.repository;

import com.dev.library.domain.Book;
import com.dev.library.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long>, BookRepositoryCustom{
	Optional<Book> findBookByTitle(String title);
	Optional<List<Book>> findBooksByUser(User user);
}
