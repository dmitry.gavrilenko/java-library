package com.dev.library.repository;

import com.dev.library.domain.Book;

import java.util.List;

public interface BookRepositoryCustom {
	List<Book> findBooksByAuthorsInAndGanresIn(List<String> authors, List<String> ganres);
}
