package com.dev.library.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class BaseModel {

	@Id
	@GeneratedValue(strategy =
			GenerationType.IDENTITY)
	private long id;

	@CreationTimestamp
	@Column(name = "created_time")
	private Date createdTime;

	@UpdateTimestamp
	@Column(name = "updated_time")
	private Date updatedTime;

	public BaseModel() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
}
