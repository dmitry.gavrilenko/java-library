package com.dev.library.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Ganre extends BaseModel{

	private String name;

	@ManyToMany(mappedBy = "ganres", fetch = FetchType.LAZY)
	private List<Book> books = new ArrayList<>();

	public Ganre() {
	}

	public Ganre(String name) {
		this.name = name;
	}

	public Ganre(String name, List<Book> books) {
		this.name = name;
		this.books = books;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
}
