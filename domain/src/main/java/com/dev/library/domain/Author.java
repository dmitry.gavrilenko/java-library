package com.dev.library.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Author extends BaseModel {

	private String name;

	private String birthday;

	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
	private List<Book> books = new ArrayList<>();

	public Author() {
	}

	public Author(String name, String birthday, List<Book> books) {
		this.name = name;
		this.birthday = birthday;
		this.books = books;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
}
