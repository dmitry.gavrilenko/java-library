package com.dev.library.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "dev_user")
public class User extends BaseModel {

	private String name;

	private String email;

	private String password;

	private String authority = "ROLE_USER";

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<Book> books = new ArrayList<>();

	public User() {
	}

	public User(String name, String email, String password, String authority) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.authority = authority;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
}
