package com.dev.library.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Book extends BaseModel {

	private String title;

	private String isbn;

	private String date;

	private int rate;

	@Column(name = "book_path")
	private String bookPath;

	@Column(name = "cover_path")
	private String coverPath;

	@ManyToOne
	private Publisher publisher;

	@ManyToOne
	private Author author;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "book_ganres",
			joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "ganre_id", referencedColumnName = "id"))
	private List<Ganre> ganres = new ArrayList<>();

	@ManyToOne
	private User user;

	public Book() {
	}

	public Book(String title, String isbn, String date, String bookPath,
				Publisher publisher, Author author, List<Ganre> ganres) {
		this.title = title;
		this.isbn = isbn;
		this.date = date;
		this.bookPath = bookPath;
		this.publisher = publisher;
		this.author = author;
		this.ganres = ganres;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public String getBookPath() {
		return bookPath;
	}

	public void setBookPath(String bookPath) {
		this.bookPath = bookPath;
	}

	public String getCoverPath() {
		return coverPath;
	}

	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Ganre> getGanres() {
		return ganres;
	}

	public void setGanres(List<Ganre> ganres) {
		this.ganres = ganres;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
