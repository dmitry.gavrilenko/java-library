package com.dev.library.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Publisher extends BaseModel {

	private String name;

	@OneToMany(mappedBy = "publisher", fetch = FetchType.LAZY)
	private List<Book> books = new ArrayList<>();

	public Publisher() {
	}

	public Publisher(String name, List<Book> books) {
		this.name = name;
		this.books = books;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
}
