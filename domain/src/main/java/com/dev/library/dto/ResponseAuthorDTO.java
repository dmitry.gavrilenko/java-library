package com.dev.library.dto;

public class ResponseAuthorDTO {

	private long id;

	private String name;

	private String birthday;

	public ResponseAuthorDTO() {
	}

	public ResponseAuthorDTO(String name, String birthday) {
		this.name = name;
		this.birthday = birthday;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
}
