package com.dev.library.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

public class BookDTO {

	private long id;

	@NotNull(message = "Title can't be empty")
	private String title;

	private String isbn;

	@NotNull(message = "Book can't be empty")
	private MultipartFile book;

	@NotNull(message = "Cover can't be empty")
	private MultipartFile cover;

	private String date;

	private String publisherName;

	private String authorName;

	private String authorBirthday;

	private List<ResponseGanreDTO> ganres;

	public BookDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public MultipartFile getBook() {
		return book;
	}

	public void setBook(MultipartFile book) {
		this.book = book;
	}

	public MultipartFile getCover() {
		return cover;
	}

	public void setCover(MultipartFile cover) {
		this.cover = cover;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getAuthorBirthday() {
		return authorBirthday;
	}

	public void setAuthorBirthday(String authorBirthday) {
		this.authorBirthday = authorBirthday;
	}

	public List<ResponseGanreDTO> getGanres() {
		return ganres;
	}

	public void setGanres(List<ResponseGanreDTO> ganres) {
		this.ganres = ganres;
	}
}
