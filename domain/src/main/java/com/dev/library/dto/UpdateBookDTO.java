package com.dev.library.dto;

import java.util.ArrayList;
import java.util.List;

public class UpdateBookDTO {

	private long id;

	private String title;

	private String isbn;

	private String date;

	private String bookPath;

	private String coverPath;

	private ResponsePublisherDTO publisher;

	private ResponseAuthorDTO author;

	private List<ResponseGanreDTO> ganres = new ArrayList<>();

	private ResponseUserDTO user;

	public UpdateBookDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBookPath() {
		return bookPath;
	}

	public void setBookPath(String bookPath) {
		this.bookPath = bookPath;
	}

	public String getCoverPath() {
		return coverPath;
	}

	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public ResponsePublisherDTO getPublisher() {
		return publisher;
	}

	public void setPublisher(ResponsePublisherDTO publisher) {
		this.publisher = publisher;
	}

	public ResponseAuthorDTO getAuthor() {
		return author;
	}

	public void setAuthor(ResponseAuthorDTO author) {
		this.author = author;
	}

	public List<ResponseGanreDTO> getGanres() {
		return ganres;
	}

	public void setGanres(List<ResponseGanreDTO> ganres) {
		this.ganres = ganres;
	}

	public ResponseUserDTO getUser() {
		return user;
	}

	public void setUser(ResponseUserDTO user) {
		this.user = user;
	}
}
