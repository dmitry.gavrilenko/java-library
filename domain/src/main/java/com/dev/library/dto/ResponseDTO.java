package com.dev.library.dto;

import org.springframework.http.HttpStatus;

public class ResponseDTO {

	private HttpStatus status;

	private String msg;

	private int code;

	public ResponseDTO() {
	}

	public ResponseDTO(HttpStatus status, String msg, int code) {
		this.status = status;
		this.msg = msg;
		this.code = code;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
