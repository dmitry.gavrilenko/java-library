package com.dev.library.dto;

import java.util.List;

public class ResponseBookDTO {

	private Long id;

	private String title;

	private String isbn;

	private String bookPath;

	private String coverPath;

	private String date;

	private ResponsePublisherDTO publisher;

	private ResponseAuthorDTO author;

	private List<ResponseGanreDTO> ganres;

	private int rate;

	private ResponseUserDTO user;

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getBookPath() {
		return bookPath;
	}

	public void setBookPath(String bookPath) {
		this.bookPath = bookPath;
	}

	public String getCoverPath() {
		return coverPath;
	}

	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public ResponsePublisherDTO getPublisher() {
		return publisher;
	}

	public void setPublisher(ResponsePublisherDTO publisher) {
		this.publisher = publisher;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public ResponseAuthorDTO getAuthor() {
		return author;
	}

	public void setAuthor(ResponseAuthorDTO author) {
		this.author = author;
	}

	public List<ResponseGanreDTO> getGanres() {
		return ganres;
	}

	public void setGanres(List<ResponseGanreDTO> ganres) {
		this.ganres = ganres;
	}

	public ResponseUserDTO getUser() {
		return user;
	}

	public void setUser(ResponseUserDTO user) {
		this.user = user;
	}
}
