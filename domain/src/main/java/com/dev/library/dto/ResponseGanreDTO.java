package com.dev.library.dto;

public class ResponseGanreDTO {

	private long id;

	private String name;

	public ResponseGanreDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ResponseGanreDTO(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
