package com.dev.library.dto;

public class ResponsePublisherDTO {

	private long id;

	private String name;

	public ResponsePublisherDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ResponsePublisherDTO(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
