package com.dev.library.dto;

import java.util.List;

public class CriteriaDTO {

	private List<String> publisherNames;

	private List<String> ganreNames;

	private List<String> authorNames;

	public CriteriaDTO() {
	}

	public List<String> getPublisherNames() {
		return publisherNames;
	}

	public void setPublisherNames(List<String> publisherNames) {
		this.publisherNames = publisherNames;
	}

	public List<String> getGanreNames() {
		return ganreNames;
	}

	public void setGanreNames(List<String> ganreNames) {
		this.ganreNames = ganreNames;
	}

	public List<String> getAuthorNames() {
		return authorNames;
	}

	public void setAuthorNames(List<String> authorNames) {
		this.authorNames = authorNames;
	}
}
