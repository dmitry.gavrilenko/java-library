package com.dev.library;

import com.dev.library.config.RootConfig;
import org.springframework.boot.SpringApplication;

public class LibraryApplication {
	public static void main(String...args){
		SpringApplication.run(RootConfig.class);
	}
}
