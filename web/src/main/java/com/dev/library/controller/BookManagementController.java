package com.dev.library.controller;

import com.dev.library.domain.User;
import com.dev.library.dto.BookDTO;
import com.dev.library.dto.UpdateBookDTO;
import com.dev.library.service.BookService;
import com.dev.library.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/secure/books")
public class BookManagementController {

	private final BookService bookService;

	private final UserService userService;

	public BookManagementController(BookService bookService, UserService userService) {
		this.bookService = bookService;
		this.userService = userService;
	}

	@PostMapping
	public ResponseEntity saveBook(@Valid BookDTO bookDTO, HttpServletRequest req) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(bookService.addBook(bookDTO, (User)req.getAttribute("user")));
	}

	@GetMapping
	public ResponseEntity getAllMyBook(HttpServletRequest req) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(userService.getAllUserBook((User)req.getAttribute("user")));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity deleteBook(@PathVariable  long id) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(userService.deleteBook(id));
	}

	@PutMapping
	public ResponseEntity updateBook(@RequestBody UpdateBookDTO updateBookDTO) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(userService.updateBook(updateBookDTO));
	}

}
