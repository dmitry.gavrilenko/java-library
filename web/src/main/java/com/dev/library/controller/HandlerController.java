package com.dev.library.controller;

import com.dev.library.dto.ResponseDTO;
import com.dev.library.exception.BooksNotFoundException;
import com.dev.library.exception.SaveFileException;
import com.dev.library.exception.UserAuthenticationException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandlerController {

	@ExceptionHandler(UserAuthenticationException.class)
	public ResponseEntity saveFileException(UserAuthenticationException e) {
		return ResponseEntity
				.status(e.getStatus())
				.contentType(MediaType.APPLICATION_JSON)
				.body(new ResponseDTO(e.getStatus(), e.getMsg(), e.getCode()));
	}

	@ExceptionHandler(SaveFileException.class)
	public ResponseEntity saveFileException(SaveFileException e) {
		return ResponseEntity
				.status(e.getStatus())
				.contentType(MediaType.APPLICATION_JSON)
				.body(new ResponseDTO(e.getStatus(), e.getMsg(), e.getCode()));
	}

	@ExceptionHandler(BooksNotFoundException.class)
	public ResponseEntity booksNotFoundException(BooksNotFoundException e) {
		return ResponseEntity
				.status(e.getStatus())
				.contentType(MediaType.APPLICATION_JSON)
				.body(new ResponseDTO(e.getStatus(), e.getMsg(), e.getCode()));
	}
}
