package com.dev.library.controller;

import com.dev.library.service.GanreService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/ganres")
public class GanreController {

	private final GanreService ganreService;

	public GanreController(GanreService ganreService) {
		this.ganreService = ganreService;
	}

	@GetMapping
	public ResponseEntity getAllGanres() {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(ganreService.findAllGanres());
	}

}
