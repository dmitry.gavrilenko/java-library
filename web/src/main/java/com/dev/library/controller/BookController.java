package com.dev.library.controller;

import com.dev.library.dto.CriteriaDTO;
import com.dev.library.service.BookService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/books")
public class BookController {

	private final BookService bookService;

	public BookController(BookService bookService) {
		this.bookService = bookService;
	}

	@GetMapping
	public ResponseEntity getAllBooks() {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(bookService.findAllBook());
	}

	@GetMapping("/{title}")
	public ResponseEntity getBook(@PathVariable String title) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_PDF)
				.body(bookService.getBookByBytes(title));
	}

	@GetMapping("/download/{title}")
	public ResponseEntity downloadBook(@PathVariable String title) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + title)
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(bookService.getBookByBytes(title));
	}

	@PostMapping("/criteria")
	public ResponseEntity findBooksByCriteria(@RequestBody CriteriaDTO criteriaDTO) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(bookService.findBooksByCriteria(criteriaDTO));
	}

	@PutMapping("/rate/{id}")
	public ResponseEntity rate(@PathVariable long id) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(bookService.rate(id));
	}
}
