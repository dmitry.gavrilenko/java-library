package com.dev.library.controller;

import com.dev.library.dto.SignUpDTO;
import com.dev.library.dto.SignInDTO;
import com.dev.library.service.ManagementUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;

@Controller
@RequestMapping("/users")
public class UserManagementController {

	private final ManagementUserService managementUserService;

	public UserManagementController(ManagementUserService managementUserService) {
		this.managementUserService = managementUserService;
	}

	@PostMapping("/sign-in")
	public ResponseEntity SignIn(@RequestBody SignInDTO signInDTO) throws UnsupportedEncodingException {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(managementUserService.signIn(signInDTO));
	}

	@PostMapping("/sign-up")
	public ResponseEntity signUp(@RequestBody SignUpDTO signUpDTO ) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(managementUserService.signUp(signUpDTO));
	}

}
