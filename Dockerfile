FROM java:8

ADD web/target/web-0.0.1-SNAPSHOT.jar web-0.0.1-SNAPSHOT.jar

CMD java -jar web-0.0.1-SNAPSHOT.jar
