package com.dev.library.provider;

import com.dev.library.service.JwtService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtProvider extends AbstractUserDetailsAuthenticationProvider {

    private JwtService jwtService;

    public JwtProvider(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                  UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(String s,
                                       UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
            throws AuthenticationException {
        String token = (String) usernamePasswordAuthenticationToken.getPrincipal();

        return jwtService.getUserDetailsFromToken(token);
    }
}
