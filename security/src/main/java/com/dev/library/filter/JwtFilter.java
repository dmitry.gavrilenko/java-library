package com.dev.library.filter;

import com.dev.library.service.JwtService;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class JwtFilter extends AbstractAuthenticationProcessingFilter {

	private JwtService jwtService;

	public JwtFilter(String defaultFilterProcessesUrl, JwtService jwtService) {
		super(defaultFilterProcessesUrl);
		this.jwtService = jwtService;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
												HttpServletResponse httpServletResponse) throws AuthenticationException {
		String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
		httpServletRequest.setAttribute("user", jwtService.getUserFromToken(token));
		return getAuthenticationManager()
				.authenticate(new UsernamePasswordAuthenticationToken(token, null));
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
											Authentication authResult) throws IOException, ServletException {
		super.successfulAuthentication(request, response, chain, authResult);
		chain.doFilter(request, response);
	}
}
