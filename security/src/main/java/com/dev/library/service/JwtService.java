package com.dev.library.service;

import com.dev.library.details.UserDetailsImpl;
import com.dev.library.domain.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

@Service
public class JwtService {

    private final String secret;

    private final int expired;

    public JwtService(@Value("${jwt.secret}") String secret,
                      @Value("${jwt.expiration}") int expired) {
        this.secret = secret;
        this.expired = expired;
    }

    public String generateToken(User user) throws UnsupportedEncodingException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, expired);
        return Jwts.builder().setExpiration(calendar.getTime())
                .signWith(SignatureAlgorithm.HS256, secret.getBytes("UTF-8"))
                .claim("id", user.getId())
                .claim("name", user.getName())
                .claim("email", user.getEmail())
                .claim("authority", user.getAuthority())
                .compact();
    }

    public UserDetails getUserDetailsFromToken(String token) {
        return new UserDetailsImpl(getUserFromToken(token));
    }

    public User getUserFromToken(String token){
        Jws<Claims> claims = parseToken(token);
        User user = new User();
        user.setId(claims.getBody().get("id", Long.class));
        user.setName(claims.getBody().get("name", String.class));
        user.setEmail(claims.getBody().get("email", String.class));
        user.setAuthority(claims.getBody().get("authority", String.class));
        return user;
    }

    private Jws<Claims> parseToken(String token) {
        Jws<Claims> claims;
        try {
            claims = Jwts.parser().setSigningKey(secret.getBytes("UTF-8")).parseClaimsJws(token);
        } catch (Exception e) {
            throw new AuthenticationCredentialsNotFoundException("Cant parse token", e);
        }
        return claims;
    }
}
