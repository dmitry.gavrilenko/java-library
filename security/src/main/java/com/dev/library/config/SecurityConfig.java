package com.dev.library.config;

import com.dev.library.filter.JwtFilter;
import com.dev.library.service.JwtService;
import com.dev.library.provider.JwtProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;
import java.util.Collections;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true)
@ComponentScan(basePackages = "com.dev.library.service")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final JwtService jwtService;

	public SecurityConfig(JwtService jwtService) {
		this.jwtService = jwtService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.cors().disable()
				.csrf().disable()
				.authorizeRequests()
				.antMatchers("/user/sign-up", "/user/sign-in").permitAll()
				.antMatchers("/secure/**").authenticated()
				.and()
				.httpBasic().disable()
				.addFilterBefore(filter(), UsernamePasswordAuthenticationFilter.class);
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManager() {
		return new ProviderManager(Collections.singletonList(authenticationProvider()));
	}

	@Bean
	public Filter filter() {
		JwtFilter filter = new JwtFilter("/secure/**", jwtService);
		filter.setAuthenticationManager(authenticationManager());
		filter.setAuthenticationSuccessHandler((req, res, auth) -> {});
		return filter;
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		return new JwtProvider(jwtService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}

